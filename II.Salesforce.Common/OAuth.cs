﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace II.Salesforce.Common
{
    public class OAuth
    {
        public string access_token { get; set; }
        public string instance_url { get; set; }
        public string id { get; set; }
        public string token_type { get; set; }
        public string issued_at { get; set; }
        public string signature { get; set; }
        public string apiversion { get; set; }
        public string restapiversion { get { return "v" + apiversion; } }
        public string bulkapiversion { get { return apiversion; } }


        private string _instance = string.Empty;
        public string instance
        {
            get
            {
                if (string.IsNullOrEmpty(_instance))
                {
                    Uri uri = new Uri(instance_url);
                    var subdomain = new StringBuilder();
                    for (var i = 0; i < uri.Host.Split(new char[] { '.' }).Length - 2; i++)
                    {
                        //Ignore any www values of ExcludeWWW option is set
                        if (uri.Host.Split(new char[] { '.' })[i].ToLowerInvariant() == "www") continue;
                        //I use a ternary operator here...this could easily be converted to an if/else if you are of the ternary operators are evil crowd
                        string ret = (i < uri.Host.Split(new char[] { '.' }).Length - 3 && uri.Host.Split(new char[] { '.' })[i + 1].ToLowerInvariant() != "www") ?
                                               uri.Host.Split(new char[] { '.' })[i] + "." :
                                               uri.Host.Split(new char[] { '.' })[i];

                        if (ret.StartsWith("cs", StringComparison.CurrentCulture) || ret.StartsWith("ap", StringComparison.CurrentCulture))
                        {
                            _instance = ret;
                            int idx = ret.LastIndexOf('.');
                            if (idx != -1) _instance = ret.Substring(0, ret.Length - 1);

                        }
                    }

                }

                return _instance;
            }
        }
    }
}
