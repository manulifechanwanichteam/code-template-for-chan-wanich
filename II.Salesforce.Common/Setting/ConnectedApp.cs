﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace II.Salesforce.Common.Setting
{
    public class ConnectedApp
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
    }
}
