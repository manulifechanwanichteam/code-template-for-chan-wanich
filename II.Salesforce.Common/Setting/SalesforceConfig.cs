﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace II.Salesforce.Common.Setting
{
    public interface ISalesforceConfig
    {
        ApiConfig ApiConfig { get; }
        Credential Credential { get; }
        ConnectedApp ConnectedApp { get; }
    }
    public class SalesforceConfig : ISalesforceConfig
    {
        private const string PASSWORD = "6aSAk9:2<%DlXfmJ==";
        private ISettingsReader _settingReader;
        private ApiConfig _apiConfig;
        private Credential _credential;
        private ConnectedApp _connectedApp;
        public SalesforceConfig(ISettingsReader settingReader)
        {
            _settingReader = settingReader;
        }
        public ApiConfig ApiConfig
        {
            get
            {
                if (null == _apiConfig)
                {
                    _apiConfig = _settingReader.LoadSection<ApiConfig>();
                }
                return _apiConfig;
            }
        }
        public Credential Credential
        {
            get
            {
                if (null == _credential)
                {
                    _credential = _settingReader.LoadSection<Credential>();
                    //Encrypt
                    //var decrypted = AES.Decrypt(_credential.Password, PASSWORD, 256);
                    _credential.Password = _credential.Password;


                }
                return _credential;
            }
        }
        public ConnectedApp ConnectedApp
        {
            get
            {
                if (null == _connectedApp)
                {
                    _connectedApp = _settingReader.LoadSection<ConnectedApp>();
                }
                return _connectedApp;
            }
        }
    }
}
