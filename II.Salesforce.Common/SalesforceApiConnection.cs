﻿using II.Salesforce.Common.Setting;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace II.Salesforce.Common
{
    public interface ISalesforceApiConnection
    {
        OAuth Login();
    }
    public class SalesforceApiConnection : ISalesforceApiConnection
    {
        private ISalesforceConfig _salesforceConfig;
        private OAuth oAuth;
        public SalesforceApiConnection(ISalesforceConfig salesforceConfig)
        {
            _salesforceConfig = salesforceConfig;
        }
        public OAuth Login()
        {
            try
            {
                var LOGIN_URL = string.Format("{0}/services/oauth2/token?grant_type=password&client_id={1}&client_secret={2}&username={3}&password={4}{5}",
                _salesforceConfig.ApiConfig.LoginUrl,
                _salesforceConfig.ConnectedApp.ConsumerKey,
                _salesforceConfig.ConnectedApp.ConsumerSecret,
                _salesforceConfig.Credential.UserName,
                _salesforceConfig.Credential.Password,
                _salesforceConfig.Credential.Token
                );

                var clientl = new RestClient(LOGIN_URL);
                var request = new RestRequest(Method.POST);
                var rest = clientl.Execute(request);
                if (rest.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    oAuth = JsonConvert.DeserializeObject<OAuth>(rest.Content);
                    oAuth.apiversion = _salesforceConfig.ApiConfig.ApiVersion;
                }
            }
            catch (Exception ex)
            {
                throw new SFClientException("Cannot login to salesforce", ex);
            }
            return oAuth;
        }
    }
    [Serializable]
    public class SFClientException : Exception
    {
        public SFClientException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
