﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace II.Salesforce.Example.SObject
{
    public class Account
    {
        // require sobject name
        public const String SObjectTypeName = "Account";

        public String Id { get; set; }
        public String Name { get; set; }
    }
}
