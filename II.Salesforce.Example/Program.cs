﻿using II.Salesforce.Common;
using II.Salesforce.Common.Setting;
using Ninject;
using System;
using System.Net;
using RestSharp;
using System.IO;
using Newtonsoft;
using Newtonsoft.Json;

namespace II.Salesforce.Example
{
    class Program
    {
        private static SystemContext _systemContext;

        static void Main(string[] args)
        {
            // Fix TLS1.2
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // Create system context
            _systemContext = new SystemContext();
            _systemContext.ConfigComponents();

            // Login 
            var client = _systemContext.kernel.Get<ISalesforceApiConnection>();
            try
            {

                // Execute an action
                var oAuth = client.Login(); // Enable Retry for login up to 3 time
                if (null != oAuth)
                {
                    var refNo = "REF001638";
                    //Read and sent to salesforce
                    var sfClient = new RestClient(oAuth.instance_url + "/services/apexrest/MMLPdfRecieveRest/"+ refNo + "/v1");
                    var request = new RestRequest(Method.POST);
                    byte[] pdfBytes;
                    using (FileStream fs = File.Open(@"D:\ux.pdf", FileMode.Open))
                    {
                        pdfBytes = new BinaryReader(fs).ReadBytes((int)fs.Length);
                    }
                    request.AddParameter("application/octet-stream", pdfBytes, ParameterType.RequestBody);
                    request.AddHeader("authorization", "Bearer " + oAuth.access_token);
                    IRestResponse response = sfClient.Execute(request);
                    string json = response.Content;
                    var results = JsonConvert.DeserializeObject<dynamic>(json);
                    string code = results.statusCode;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {

            }
            Console.WriteLine("Complete .........");
            Console.ReadKey();
        }
    }    
    public class SystemContext
    {
        public IKernel kernel;
        public void ConfigComponents()
        {
            kernel = new StandardKernel();
            kernel.Bind<ISettingsReader>().To<SettingsReader>().InSingletonScope().WithConstructorArgument("configurationFilePath", "salesforce.json");
            kernel.Bind<ISalesforceConfig>().To<SalesforceConfig>();
            kernel.Bind<ISalesforceApiConnection>().To<SalesforceApiConnection>();
        }
    }
}
